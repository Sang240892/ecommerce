<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@assetsUploadPatch', dirname(dirname(__DIR__)) . '/public/upload');


Yii::setAlias('@assetsUrl', 'http://ecommerce.cdn.com/assets');
Yii::setAlias('@assetsUrlImageLoginPage', 'http://ecommerce.cdn.com/assets/images/page/login');

Yii::setAlias('@assetsScript', 'http://ecommerce.cdn.com/assets/scripts');
Yii::setAlias('@assetsStyles', 'http://ecommerce.cdn.com/assets/styles');

Yii::setAlias('@assetsUploadUrl', 'http://ecommerce.cdn.com/upload');


