<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
$asset		= backend\assets\AppAsset::register($this);

$this->title = $name;

$baseUrl 	= $asset->baseUrl;
$dashboard  = (new \yii\web\Request())->getBaseUrl();
$jsScripts = <<<JS

JS;
$this->registerCssFile(Yii::$app->homeUrl.'css/site.css');
$this->registerJs($jsScripts, \yii\web\View::POS_READY, $key = null);
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12 page-404">
		<h3><?php echo $message;?></h3>
	</div>
</div>
<div class="row">
	<div class="col-md-12 page-404">
		<div class="number">
			 <?= Html::encode($this->title) ?>
		</div>
		<div class="details" style="height: 800px;">
			<h3>Oops! You're lost.</h3>
			<p>
				We can not find the page you're looking for.<br/>
				<a href="/dashboard">
				Return home </a>
				or try the search bar below.
			</p>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->