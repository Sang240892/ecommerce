<?php
/**
 * User: sangnguyen on  10/20/15 at 00:35
 * File name: extra_profile.php
 * Project name: ysd-tee-shirt
 * Copyright (c) 2015 by YSD
 * All rights reserved
 */
use yii\widgets\Breadcrumbs;
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            <?php echo $this->title;?>
        </h3>
        <div class="page-bar">
            <?php
            echo Breadcrumbs::widget([
                'options' => [
                    'class' => 'page-breadcrumb'
                ],
                'homeLink' => [
                    'label' => Yii::t('yii', 'Dashboard'),
                    'url' => Yii::$app->homeUrl,
                    'template' => '<li><i class="fa fa-home"></i> {link}<i class="fa fa-angle-right"></i></li>'
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
            ])?>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row margin-bottom-40">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="portlet light profile-sidebar-portlet">
                        <!-- SIDEBAR USERPIC -->
                        <div class="" style="margin-left: 13%">
                            <?php $df_image = "http://local.fitroad-assets.com/assets/df_i/default_avatar_undefined.png"; ?>
                            <img style="width: 250px;" src="<?php echo $avatar ?  \api\commons\helpers\ApiHelpers::builtUrlImages(Yii::$app->params['avatarsFinder'],$avatar->url,Yii::$app->params['normalName']) : $df_image;?>" alt="" />
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                               <?php  echo trim($userRequest->first_name .' '.$userRequest->last_name);?>
                            </div>
                            <div class="profile-usertitle-job">
                                <?php echo \backend\commons\helpers\UtilHelper::getRoleName($userRequest->role);
                                ?>
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
<!--                        <div class="profile-userbuttons">-->
<!--                            <button type="button" class="btn btn-circle green-haze btn-sm">Follow</button>-->
<!--                            <button type="button" class="btn btn-circle btn-danger btn-sm">Message</button>-->
<!--                        </div>-->
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <?= $this->render('sidebar_profile.php',['baseUrl'=>$baseUrl,'userRequest'=>$userRequest]);?>
                        <!-- END MENU -->
                    </div>
                    <!-- END PORTLET MAIN -->
                    <!-- PORTLET MAIN -->
                    <!-- END PORTLET MAIN -->
                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <?php echo $content?>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>

    </div>
</div>