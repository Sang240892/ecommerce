<!--
 * User: sangnguyen on  10/20/15 at 00:50
 * File name: sidebar_profile_management.php
 * Project name: ysd-tee-shirt
 * Copyright (c) 2015 by YSD
 * All rights reserved
 *
-->
<div class="profile-usermenu">
<?php
$menuItems = [
    [
        'label' => 'Dashboard',
        'url' => (Yii::$app->user->can('User-Manage-View'))?['/user/manage/view', 'id' => $userRequest->id]:['/user/manage/profile-view', 'id' => $userRequest->id],
        'template' => '<a href="{url}"><i class="fa fa-dashboard "></i><span class="title">{label}</span></a>'
    ],
    [
        'label' => 'Activities',
        //'url' => ['/user/manage/activity', 'id' => $userRequest->id],
        'url'=> 'javascript:;',
        'template' => '<a href="{url}"><i class="fa fa-tasks"></i><span class="title">{label}</span></a>'
    ],
    [
        'label' => \Yii::t('backend','Account settings'),
        'url' => (Yii::$app->user->can('User-Manage-Update'))?['/user/manage/update', 'id' => $userRequest->id]:['/user/manage/profile', 'id' => $userRequest->id],
        'template' => '<a href="{url}"><i class="icon-settings"></i><span class="title">{label}</span></a>'
    ],
];

echo  \yii\widgets\Menu::widget([
    'options' => [
        'class' => 'nav',
        'data-keep-expanded' => false,
        'data-auto-scroll' => true,
        'data-slide-speed' => '200'
    ],
    'items' => $menuItems,
    'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
    'encodeLabels' => false, // allows you to use html in labels
    'activateParents' => true,
    'activeCssClass' => 'active'
]);
?>
</div>
