<?php
// use kartik\sidenav\SideNav;
use yii\widgets\Menu;
use yii\helpers\Url;
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		 <?php
         $menuItems = [
    [
        'options' => [
            'class' => 'sidebar-toggler-wrapper'
        ],
        'template' => '<!-- BEGIN SIDEBAR TOGGLER BUTTON --><div class="sidebar-toggler"></div><!-- END SIDEBAR TOGGLER BUTTON -->'
    ],
    [
        'options' => [
            'class' => 'sidebar-search-wrapper'
        ],
        'template' => '
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <p></p>
          <!--  <form class="sidebar-search " action="extra_search.html" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                    </span>
                </div>
            </form>-->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->'
    ],
//    [
//        'label' => 'Dashboard',
//        'url' => 'javascript:;',
//        'template' => '<a href="{url}"><i class="icon-home"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
//        'items' => [
//            [
//                'label' => 'Dashboard1',
//                'class'=>'active',
//                'url' => [
//                   '/'
//                ],
//                'template' => '<a href="{url}"><i class="icon-home"></i><span class="title">{label}</span></a>'
//            ],
//            [
//                'label' => 'Dashboard2',
//                'url' => [
//                    '/'
//                ],
//                'template' => '<a href="{url}"><i class="icon-home"></i><span class="title">{label}</span></a>'
//            ],
//            [
//                'label' => 'Manager Dashboard',
//                'url' => [
//                    '/'
//                ],
//                'template' => '<a href="{url}"><i class="icon-home"></i><span class="title">{label}</span></a>'
//            ]
//        ]
//    ],
             (Yii::$app->user->can('report') || Yii::$app->user->can('report'))?[
                 'label' => \Yii::t('backend','Dashboard'),
                 'active'=> Yii::$app->controller->module->id == 'report',
                 'url' => [Url::to('/report/manage/index')],
                 'template'=> '<a href="{url}"><i class="fa fa-dashboard"></i><span class="selected"></span><span class="title">{label}</span></a>',

         ]:FALSE,
             (Yii::$app->user->can('food-manage-index') || Yii::$app->user->can('food'))?[
                    'label' => \Yii::t('backend','Foods'),
                    'active'=> Yii::$app->controller->module->id == 'food',
                    'url'	=> Url::to('javascript:void();'),
                    'template'=> '<a href="{url}"><i class="fa fa-cutlery"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
                         'items' => [
                             [
                                 'label' =>'<i class="fa fa-list"></i> '. \Yii::t('backend','List'),
                                 'url' => [Url::to('/food/manage/index')],
                                 'template' => '<a href="{url}"><span class="title">{label}</span></a>'
                             ],
                             (Yii::$app->user->can('restaurant-manage-google-search'))?[
                                 'label' =>'<i class=" fa fa-download"></i> '. \Yii::t('backend','MacDonal Crawling'),
                                 'url' => [Url::to('/food/crawl/mac-donal')],
                                 'template' => '<a href="{url}"><span class="title">{label}</span></a>'
                             ]:FALSE
                         ]
         ]:FALSE,   
             ( Yii::$app->user->can('restaurant-manage-index') || Yii::$app->user->can('restaurant'))?[
         'label' => \Yii::t('backend','Restaurants'),
         'active'=> Yii::$app->controller->module->id == 'restaurant',
         'url'	=> Url::to('javascript:void();'),
         'template'=> '<a href="{url}"><i class="fa fa-institution"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
         'items' => [
             [
                 'label' =>'<i class="fa fa-list"></i> '. \Yii::t('backend','List'),
                 'url' => [Url::to('/restaurant/manage/index')],
                 'template' => '<a href="{url}"><span class="title">{label}</span></a>'
             ],
             (Yii::$app->user->can('restaurant-manage-google-search'))?[
                 'label' =>'<i class=" fa fa-search"></i> '. \Yii::t('backend','Search'),
                 'url' => [Url::to('/restaurant/manage/google-search')],
                 'template' => '<a href="{url}"><span class="title">{label}</span></a>'
             ]:FALSE
         ]
     ]:FALSE,
             (Yii::$app->user->can('user-manage-index') || Yii::$app->user->can('user'))?[
        'label' => \Yii::t('backend','Users'),
        'active'=>Yii::$app->controller->module->id == 'user',
        'url'	=> Url::to('javascript:void();'),
        'template'=> '<a href="{url}"><i class="icon-user"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
        'items' => [
            [
                'label' =>'<i class="icon-user"></i> '. \Yii::t('backend','Members'),
                'url' => [Url::to('/user/manage/index')],
                'template' => '<a href="{url}"><span class="title">{label}</span></a>'
            ],
            (Yii::$app->user->can('user-manage-staff') || Yii::$app->user->can('user'))?[
                'label' =>'<i class="icon-user"></i> '. \Yii::t('backend','Employees'),
                'url' => [Url::to('/user/manage/staff')],
                'template' => '<a href="{url}"><span class="title">{label}</span></a>'
            ]:FALSE
        ]
    ]:FALSE,
    Yii::$app->user->can('authorization')?[
		'label' => 'Authorization',
		'url'	=> Url::to('javascript:void();'),
        'active'=> Yii::$app->controller->module->id == 'authorization',
		'template'=> '<a href="{url}"><i class="fa fa-lock"></i><span class="selected"></span><span class="title">{label}</span><span class="arrow"></span></a>',
		'items' => [
			[
				'label' =>'<i class="fa fa-unlock-alt"></i>'. \Yii::t('backend','Permission'),
				'url' => [Url::to('/authorization/permission/index')],
			]
		]
	]:FALSE,
     (Yii::$app->user->can('terms') || Yii::$app->user->can('terms'))?[
         'label' => \Yii::t('backend','Terms and Conditions'),
         'active'=> Yii::$app->controller->module->id == 'terms',
         'url' => [Url::to('/terms/manage/index')],
         'template'=> '<a href="{url}"><i class="fa fa-copyright"></i><span class="selected"></span><span class="title">{label}</span></a>',

     ]:FALSE,
     (Yii::$app->user->can('app') || Yii::$app->user->can('app'))?[
         'label' => \Yii::t('backend','Push notifications'),
         'active'=> Yii::$app->controller->module->id == 'app',
         'url' => [Url::to('/app/default/push-notifications')],
         'template'=> '<a href="{url}"><i class="fa fa-bell"></i><span class="selected"></span><span class="title">{label}</span></a>',

     ]:FALSE,

];
echo Menu::widget([
    'options' => [
        'class' => 'page-sidebar-menu',
        'data-keep-expanded' => FALSE,
        'data-auto-scroll' => TRUE,
        'data-slide-speed' => '200'
    ],
    'items' => $menuItems,
    'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
    'encodeLabels' => FALSE, // allows you to use html in labels
    'activateParents' => TRUE,
    'activeCssClass' => 'active'
]);
?>
    
	</div>
</div>