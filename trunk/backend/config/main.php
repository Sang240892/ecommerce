<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
use \yii\web\Request;

$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'auth' => [
            'class' => 'backend\modules\auth\Module',
        ],
    ],
    'components' => [

        'request' => [
            'baseUrl' => $baseUrl,
        ],
        'user' => [
            'identityClass' => 'common\models\UserIdentity',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/authenticate/verify'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
                //'/auth/reset-password/<token:[\w\-]+>'                    => 'auth/authenticate/reset-password',
                //                'permission/<action:[\w\-]+>/<id:\d+>'           => 'permission/permission/<action>',
            ],
        ],
        'view' => [
            'theme'=>[
                'pathMap' => ['@app/views' => '@webroot/themes/ecommerce/views'],
            ],
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'layout'=>'@webroot/themes/ecommerce/views/layouts/main.php',
    'params' => $params,
    'aliases' => [
        // Set the editor language dir
        '@uploadPathName' => '/public/upload',
        '@rootPath' => realpath(dirname(__FILE__).'/../../'),
    ],
];
